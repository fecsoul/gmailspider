var YOUR_CLIENT_ID = '769593058678-udo09hnj2a4hmucinfm9bpoj5b8gdrna.apps.googleusercontent.com';
var YOUR_REDIRECT_URI = 'https://gmailspider.onrender.com/';
// var YOUR_REDIRECT_URI = 'http://127.0.0.1:5500';

var fragmentString = location.hash.substring(1);
var params = {};
var regex = /([^&=]+)=([^&]*)/g, m;
while (m = regex.exec(fragmentString)) {
    params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
}

if (Object.keys(params).length > 0) {
    localStorage.setItem('oauth2-test-params', JSON.stringify(params));
    if (params['state'] && params['state'] == 'getSpreadsheetIds') {
        getSpreadsheetIds();
    }
}

async function google_request_token() {
    const maxMessages = 10
    var params = JSON.parse(localStorage.getItem('oauth2-test-params'));
    if (params && params['access_token']) {
        try {
            var messages = localStorage.getItem('gmail-messages');
            if (!messages) {
                var response = await fetch('https://www.googleapis.com/gmail/v1/users/me/messages?maxResults=' + maxMessages + '&access_token=' + params['access_token']);
                if (response.ok) {
                    var data = await response.json();
                    messages = JSON.stringify(data.messages);
                    localStorage.setItem('gmail-messages', messages);
                } else {
                    throw new Error('Network response was not ok.');
                }
            }

            getMessagesDetails(JSON.parse(messages), params['access_token']);
        } catch (error) {
            console.error('Fetch error:', error);
            oauth2SignIn();
        }
    } else {
        oauth2SignIn();
    }
}
async function getSpreadsheetIds() {
    var params = JSON.parse(localStorage.getItem('oauth2-test-params'));
    
    if (params && params['access_token']) {
        try {
            var response = await fetch(`https://www.googleapis.com/drive/v3/files?q=mimeType='application/vnd.google-apps.spreadsheet'&access_token=${params['access_token']}`);
            
            if (response.ok) {
                var data = await response.json();
                var spreadsheetIds = data.files.map(file => file.id);
                console.log('IDs de hojas de cálculo:', spreadsheetIds);
            } else {
                throw new Error('Network response was not ok.');
            }
        } catch (error) {
            console.error('Fetch error:', error);
            oauth2SignIn();
        }
    } else {
        oauth2SignIn();
    }
}



async function getMessageDetails(message, accessToken) {
    var categorizedMessages = JSON.parse(localStorage.getItem('categorizedMsg'));
    if (categorizedMessages){
    var response = await fetch('https://www.googleapis.com/gmail/v1/users/me/messages/' + message.id + '?access_token=' + accessToken) ;
            if (response.ok) {
                var messageDetails = await response.json();
                let from = messageDetails.payload.headers.find(header => header.name === 'From').value;
                let subject = messageDetails.payload.headers.find(header => header.name === 'Subject').value;
                let date = messageDetails.payload.headers.find(header => header.name === 'Date').value;
                var fecha = new Date(date);

                var opciones = { timeZone: 'America/Argentina/Buenos_Aires', weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' };
                var fechaFormateada = fecha.toLocaleDateString('es-ES', opciones);
                var partes = fechaFormateada.split(' ');
                var diaSemana = partes[0].slice(0,-1);

                var dia = partes[1];
                var mes = partes[3];
                var anio = partes[5].slice(0,-1);
                var hora = partes[6];

                let body = messageDetails.snippet;

                categorizedMessages['De'].push({ from: from });
                categorizedMessages['Título'].push({ subject: subject });
                categorizedMessages['Mensaje'].push({ body: body });
                categorizedMessages['DiaSemana'].push({ diaSemana: diaSemana });
                
                categorizedMessages['Dia'].push({ dia: dia });
                categorizedMessages['Mes'].push({ mes: mes });
                categorizedMessages['Anio'].push({ anio: anio });
                categorizedMessages['Hora'].push({ hora: hora });
                categorizedMessages['Fecha'].push({ fecha: fecha });
                localStorage.setItem('categorizedMsg', JSON.stringify(categorizedMessages))

                await updateTableWithCategorizedMessages(categorizedMessages);

            } else {
                throw new Error('Network response was not ok.');
            }
        }

}
async function getMessagesDetails(messages, accessToken) {
    var categorizedMessages = {
        'De': [],
        'Título': [],
        'Mensaje': [],
        'DiaSemana':[],
        'Dia':[],
        'Mes':[],
        'Anio':[],
        'Hora':[],
        'Fecha':[],
        'Adjunto':[]

    };
    var messagesLocalStorage = JSON.parse(localStorage.getItem('categorizedMsg'));
    if (!messagesLocalStorage){
    for (const message of messages) {
        try {
    
            var response = await fetch('https://www.googleapis.com/gmail/v1/users/me/messages/' + message.id + '?access_token=' + accessToken) ;
            if (response.ok) {
                var messageDetails = await response.json();
                let from = messageDetails.payload.headers.find(header => header.name === 'From').value;
                let subject = messageDetails.payload.headers.find(header => header.name === 'Subject').value;
                let date = messageDetails.payload.headers.find(header => header.name === 'Date').value;
                var fecha = new Date(date);

                var opciones = { timeZone: 'America/Argentina/Buenos_Aires', weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' };
                var fechaFormateada = fecha.toLocaleDateString('es-ES', opciones);
                var partes = fechaFormateada.split(' ');
                var diaSemana = partes[0].slice(0,-1);

                var dia = partes[1];
                var mes = partes[3];
                var anio = partes[5].slice(0,-1);
                var hora = partes[6];

                let body = messageDetails.snippet;
                categorizedMessages['De'].push({ from: from });
                categorizedMessages['Título'].push({ subject: subject });
                categorizedMessages['Mensaje'].push({ body: body });
                categorizedMessages['DiaSemana'].push({ diaSemana: diaSemana });
                
                categorizedMessages['Dia'].push({ dia: dia });
                categorizedMessages['Mes'].push({ mes: mes });
                categorizedMessages['Anio'].push({ anio: anio });
                categorizedMessages['Hora'].push({ hora: hora });
                categorizedMessages['Fecha'].push({ fecha: fecha });

                var attachmentContent = null
                if (messageDetails.payload.parts && messageDetails.payload.parts.length > 0) {
            for (const part of messageDetails.payload.parts) {
                if (part.filename && part.body.attachmentId) {
                    // Si es un archivo adjunto, recupera su contenido
                    attachmentContent = await getAttachment(message.id, part.body.attachmentId, accessToken);
                    if (!attachmentContent){
                        categorizedMessages['Adjunto']
                    .push({ adjunto: 'Sin atach' });
                    }
                    categorizedMessages['Adjunto']
                    .push({ adjunto: attachmentContent });
                }
                
                }
                if (attachmentContent == null){
                    categorizedMessages['Adjunto']
                    .push({ adjunto: 'Sin atach' });
                }
            }
          } 
        } catch (error) {
            console.error('Fetch error:', error);
            oauth2SignIn();
        }
    }
}
    if (messagesLocalStorage){
        await updateTableWithCategorizedMessages(messagesLocalStorage);
    }
    else{
    localStorage.setItem('categorizedMsg', JSON.stringify(categorizedMessages))

    await updateTableWithCategorizedMessages(categorizedMessages);
}
}
async function getAttachment(messageId, attachmentId, accessToken) {
try {
const response = await fetch(`https://www.googleapis.com/gmail/v1/users/me/messages/${messageId}/attachments/${attachmentId}?access_token=${accessToken}`);
if (!response.ok) {
    return 'Not attachment'
}
const attachmentData = await response.json(); // Obtiene los datos del adjunto como objeto JSON
const attachmentContent = attachmentData.data; // Obtiene los datos binarios de la imagen
return `<img src="data:image/jpeg;base64,${attachmentContent}" />`;
} catch (error) {
console.error('Error retrieving attachment:', error);
return 'Not atach';
}
}
function updateTableWithCategorizedMessages(categorizedMessages) {
 
    table = $('#tableBody').DataTable({   language: {
        url: '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json' // Utiliza el archivo de idioma español
     } ,columns: [
    null, 
    null, 
    null, 
    null, 

    { type: 'numeric' }, 
    { type: 'numeric' }, 
    { type: 'numeric' }, 
    { type: 'time' },
    { type: 'date' },
    { type: 'html' }

],             
});
    table.clear();
    $('#tableBody').DataTable().order([8, 'desc']);
    var imagenHTML =''
    var adjunto =''
    for (var i = 0; i < categorizedMessages['De'].length; i++) {
         imagenHTML = '<img src="' + 'https://img.freepik.com/vector-premium/cartel-que-dice-que-no-hay-fotos_602333-330.jpg' + '" style="max-width: 100px; max-height: 100px;" />';
         adjunto = categorizedMessages['Adjunto'][i].adjunto == 'Sin atach'? imagenHTML: categorizedMessages['Adjunto'][i].adjunto 
        
        table.row.add([
            categorizedMessages['De'][i].from,
            categorizedMessages['Título'][i].subject,
            categorizedMessages['Mensaje'][i].body,
            categorizedMessages['DiaSemana'][i].diaSemana,

            categorizedMessages['Dia'][i].dia,
            categorizedMessages['Mes'][i].mes,
            categorizedMessages['Anio'][i].anio,
            categorizedMessages['Hora'][i].hora,
            categorizedMessages['Fecha'][i].fecha,
            adjunto
        ]).draw();
    }

}
function oauth2SignIn() {
    var oauth2Endpoint = 'https://accounts.google.com/o/oauth2/v2/auth';
    var form = document.createElement('form');
    form.setAttribute('method', 'GET');
    form.setAttribute('action', oauth2Endpoint);

    var params = {
        'client_id': YOUR_CLIENT_ID,
        'redirect_uri': YOUR_REDIRECT_URI,
        'scope': 'https://www.googleapis.com/auth/drive.readonly https://www.googleapis.com/auth/spreadsheets.readonly', // Agrega los permisos necesarios para Drive y Sheets
        'state': 'getSpreadsheetIds',
        'include_granted_scopes': 'true',
        'response_type': 'token'
    };

    for (var p in params) {
        var input = document.createElement('input');
        input.setAttribute('type', 'hidden');
        input.setAttribute('name', p);
        input.setAttribute('value', params[p]);
        form.appendChild(input);
    }

    document.body.appendChild(form);
    form.submit();
}
async function verifyLastMessage() {
    try {
        var mensajeNuevo = false
        var lastMessage =''
        var lastMessageLocal=''
        var params = JSON.parse(localStorage.getItem('oauth2-test-params'));   
        var messages = JSON.parse(localStorage.getItem('gmail-messages'));
            if (messages) {
                var response = await fetch('https://www.googleapis.com/gmail/v1/users/me/messages?maxResults=' + 1 + '&access_token=' + params['access_token']);
                if (response.ok) {
                    var data = await response.json();
                    lastMessage = data.messages[0].id;
                    lastMessageLocal = messages[messages.length-1].id
                    if(lastMessage != lastMessageLocal){
                        mensajeNuevo = true
                        messages.push(data.messages[0])
                        localStorage.setItem('gmail-messages', JSON.stringify(messages));
                        if ($.fn.DataTable.isDataTable('#tableBody')) {
                         $('#tableBody').DataTable().destroy();
                        }
                        getMessageDetails(data.messages[0],params['access_token'])
                    }

                } else {
                    throw new Error('Network response was not ok.');
                }
            }
        }
            catch (error) {
            console.error('Fetch error:', error);
        }
}


setInterval(verifyLastMessage, 3000)
